package org.c4.eventstore;

/**
 * Sammlung von ErrorKeys zur Beschreibung von API-Fehlern.
 * <br/>
 * Copyright: Copyright (c) 03.01.2020 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class ErrorKey
{
    public static final String MISSING_EVENT_TYPES = "MISSING_EVENT_TYPES";

    private ErrorKey()
    {
        throw new IllegalStateException("Utility class");
    }
}
