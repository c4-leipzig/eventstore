package org.c4.eventstore.configuration;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.c4.eventstore.aggregatelock.persistence.AggregateLockRepository;
import org.c4.eventstore.domainevent.DomainEvent;
import org.c4.eventstore.domainevent.persistence.DomainEventRepository;
import org.c4.eventstore.domainevent.DomainEventSink;
import org.c4.eventstore.messaging.EventPublisher;
import org.c4.eventstore.messaging.EventPublisherImpl;
import org.c4.eventstore.snapshotting.SnapshottingService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.*;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

/**
 * Konfiguration für asynchrones Messaging mit Kafka.
 * Der {@link EventPublisher} veröffentlicht {@link DomainEvent}s, die {@link DomainEventSink} empfängt diese.
 * <br/>
 * Copyright: Copyright (c) 17.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
@Profile("!test")
public class MessagingConfig
{
    @Value("${spring.kafka.producer.domainEventFromEventStoreTopic}")
    private String domainEventFromEventStoreTopic;
    @Value("${spring.kafka.producer.versionMismatchFromEventStoreTopic}")
    private String versionMismatchFromEventStoreTopic;

    @Bean
    public ProducerFactory<String, DomainEvent> producerFactory()
    {
        JsonSerializer<DomainEvent> jsonSerializer = new JsonSerializer<>();
        jsonSerializer.setAddTypeInfo(false);
        return new DefaultKafkaProducerFactory<>(producerConfigs(), new StringSerializer(), jsonSerializer);
    }

    private Map<String, Object> producerConfigs()
    {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);

        return props;
    }

    @Bean
    public KafkaTemplate<String, DomainEvent> kafkaTemplate(
            ProducerFactory<String, DomainEvent> producerFactory)
    {
        return new KafkaTemplate<>(producerFactory);
    }

    @Bean
    public ConsumerFactory<String, DomainEvent> consumerFactory()
    {
        return new DefaultKafkaConsumerFactory<>(consumerConfigs(), new StringDeserializer(),
                new JsonDeserializer<>(DomainEvent.class));
    }

    private Map<String, Object> consumerConfigs()
    {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "archetypeGroup");

        return props;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, DomainEvent> kafkaListenerContainerFactory(
            ConsumerFactory<String, DomainEvent> consumerFactory)
    {
        ConcurrentKafkaListenerContainerFactory<String, DomainEvent> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory);

        return factory;
    }

    @Bean
    public EventPublisher successEventPublisher(KafkaTemplate<String, DomainEvent> kafkaTemplate)
    {
        return new EventPublisherImpl(kafkaTemplate, domainEventFromEventStoreTopic);
    }

    @Bean
    public EventPublisher versionMismatchEventPublisher(KafkaTemplate<String, DomainEvent> kafkaTemplate)
    {
        return new EventPublisherImpl(kafkaTemplate, versionMismatchFromEventStoreTopic);
    }

    @Bean
    public DomainEventSink domainEventSink(SnapshottingService snapshottingService,
            DomainEventRepository domainEventRepository, AggregateLockRepository aggregateLockRepository,
            EventPublisher successEventPublisher, EventPublisher versionMismatchEventPublisher)
    {
        return new DomainEventSink(snapshottingService, domainEventRepository, aggregateLockRepository,
                successEventPublisher, versionMismatchEventPublisher);
    }
}
