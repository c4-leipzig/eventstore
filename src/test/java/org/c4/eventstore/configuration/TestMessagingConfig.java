package org.c4.eventstore.configuration;

import org.c4.eventstore.aggregatelock.persistence.AggregateLockRepository;
import org.c4.eventstore.domainevent.persistence.DomainEventRepository;
import org.c4.eventstore.domainevent.DomainEventSink;
import org.c4.eventstore.messaging.EventPublisher;
import org.c4.eventstore.messaging.MockEventPublisher;
import org.c4.eventstore.snapshotting.SnapshottingService;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;

/**
 * Konfiguration zur Überbrückung des Kafkas für Tests.
 * Relevante Schnittstellen ({@link EventPublisher}) werden gemockt.
 * <br/>
 * Copyright: Copyright (c) 22.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@TestConfiguration
public class TestMessagingConfig
{
    @Bean
    public EventPublisher successEventPublisher()
    {
        return new MockEventPublisher();
    }

    @Bean
    public EventPublisher versionMismatchEventPublisher()
    {
        return new MockEventPublisher();
    }

    @Bean
    public DomainEventSink domainEventSink(SnapshottingService snapshottingService,
            DomainEventRepository domainEventRepository, AggregateLockRepository aggregateLockRepository,
            EventPublisher successEventPublisher, EventPublisher versionMismatchEventPublisher)
    {
        return new DomainEventSink(snapshottingService, domainEventRepository, aggregateLockRepository,
                successEventPublisher, versionMismatchEventPublisher);
    }
}
