package org.c4.eventstore.aggregatelock.persistence;

import org.c4.eventstore.aggregatelock.AggregateLockDto;

/**
 * Repository zur Verwaltung von {@link AggregateLockDto}s.
 * <br/>
 * Copyright: Copyright (c) 19.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface AggregateLockRepository
{
    /**
     * Ermittelt den Lockstatus für ein Aggregat und setzt ihn.
     */
    boolean isNotLockedThenUpdate(String aggregateId);

    /**
     * Löst den Lockstatus für ein Aggregat.
     */
    void unlock(String aggregateId);
}
