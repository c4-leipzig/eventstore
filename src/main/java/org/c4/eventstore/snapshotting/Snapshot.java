package org.c4.eventstore.snapshotting;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.c4.eventstore.domainevent.DomainEvent;
import org.c4.eventstore.domainevent.IDomainEvent;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashMap;
import java.util.Map;

/**
 * DTO zur Verwaltung eines Snapshots. Größtenteils identisch zu einem {@link DomainEvent}.
 * <br/>
 * Copyright: Copyright (c) 23.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Document(collection = "snapshots")
@Getter
@Setter
public class Snapshot implements IDomainEvent
{
    @Id
    private String aggregateId;
    private Long   version;
    private String classIdentifier;

    @Getter(value = AccessLevel.NONE)
    @Setter(value = AccessLevel.NONE)
    private Map<String, Object> payload = new HashMap<>();

    @JsonIgnore
    public void setPayload(Map<String, Object> payload)
    {
        this.payload = payload;
    }

    @JsonAnySetter
    public void setPayload(String name, Object value)
    {
        payload.put(name, value);
    }

    @JsonAnyGetter
    public Map<String, Object> getPayload()
    {
        return payload;
    }
}
