package org.c4.eventstore.domainevent;

import org.bson.types.ObjectId;
import org.c4.eventstore.domainevent.persistence.SpringDataDomainEventRepository;
import org.c4.eventstore.snapshotting.Snapshot;
import org.c4.eventstore.snapshotting.persistence.SpringDataSnapshotRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
class DomainEventServiceTest
{
    @Autowired
    private SpringDataDomainEventRepository domainEventRepository;

    @Autowired
    private SpringDataSnapshotRepository snapshotRepository;

    @Autowired
    private DomainEventService domainEventService;

    private final String TEST_PROPERTY = "testProperty";
    private final String TEST_PROPERTY_2 = "testProperty2";

    @BeforeEach
    void setUp()
    {
        domainEventRepository.deleteAll();
        snapshotRepository.deleteAll();
    }

    /**
     * Alle Versionen des Aggregats werden korrekt zurückgeliefert.
     */
    @Test
    void getDomainEventForAggregateId()
    {
        DomainEvent d1 = createDomainEvent(1L);
        DomainEvent d2 = createDomainEvent(d1.getAggregateId(), 2L);
        DomainEvent d3 = createDomainEvent(0L);
        domainEventRepository.insert(d1);
        domainEventRepository.insert(d2);
        domainEventRepository.insert(d3);

        Page<DomainEvent> domainEvents = domainEventService.getDomainEventsForAggregateId(d1.getAggregateId(),
                PageRequest.of(0, 20, Sort.by("aggregateId")));
        List<Long> resultVersions = domainEvents.get().map(DomainEvent::getVersion)
                .collect(Collectors.toList());

        assertThat(domainEvents.getTotalElements(), is(equalTo(2L)));
        assertThat(resultVersions, contains(1L, 2L));
    }

    /**
     * Gesamter EventStream (ohne Snapshot) wird zurückgegeben.
     */
    @Test
    void getEventStreamWithoutSnapshot()
    {
        DomainEvent d1 = createDomainEvent(1L);
        DomainEvent d2 = createDomainEvent(d1.getAggregateId(), 2L);
        DomainEvent d3 = createDomainEvent(d1.getAggregateId(), 3L);
        domainEventRepository.insert(d1);
        domainEventRepository.insert(d2);
        domainEventRepository.insert(d3);

        List<Long> result = domainEventService.getEventStreamForAggregateId(d1.getAggregateId())
                .stream().map(IDomainEvent::getVersion).collect(Collectors.toList());

        assertThat(result.size(), is(equalTo(3)));
        assertThat(result, contains(1L, 2L, 3L));
    }

    /**
     * Wird der Eventstream angefordert, muss ein Snapshot korrekt eingebunden werden,
     * auch wenn ein Snapshot fehlen sollte.
     * Es werden nur Events geladen, die nach dem Snapshot erstellt wurden.
     */
    @Test
    void getEventStreamWithSnapshot()
    {
        DomainEvent d1 = createDomainEvent(1L);
        DomainEvent d2 = createDomainEvent(d1.getAggregateId(), 2L);
        d2.getPayload().put(TEST_PROPERTY_2, TEST_PROPERTY_2);
        DomainEvent d3 = createDomainEvent(d1.getAggregateId(), 3L);
        domainEventRepository.insert(d1);
        domainEventRepository.insert(d2);
        domainEventRepository.insert(d3);
        Snapshot snapshot = createSnapshot(d1.getAggregateId(), 2L);
        snapshot.getPayload().put(TEST_PROPERTY, TEST_PROPERTY);
        snapshotRepository.save(snapshot);

        List<IDomainEvent> result = domainEventService.getEventStreamForAggregateId(d1.getAggregateId());

        assertThat(result.size(), is(equalTo(2)));
        assertTrue(result.get(0) instanceof Snapshot);
        assertTrue(result.get(1) instanceof DomainEvent);
        assertThat(result.get(1).getVersion(), is(equalTo(3L)));
        assertThat(result.get(0).getPayload().get(TEST_PROPERTY), is(equalTo(TEST_PROPERTY)));
        assertNull(result.get(0).getPayload().get(TEST_PROPERTY_2));
    }

    /**
     * Resultat der Query wird korrekt gefüllt.
     */
    @Test
    void getAggregateIdsForEventTypes()
    {
        DomainEvent d1 = createDomainEvent(1L);
        d1.setClassIdentifier("CREATED");
        DomainEvent d2 = createDomainEvent(1L);
        d2.setClassIdentifier("ALTERED");
        domainEventRepository.insert(Arrays.asList(d1, d2));

        DomainEventAggregateIdQuery query = new DomainEventAggregateIdQuery();
        query.setEventTypes(Collections.singletonList("CREATED"));

        List<String> result = domainEventService.getAggregateIdsForEventTypes(query);


        assertThat(result.size(), is(equalTo(1)));
        assertThat(result.get(0), is(equalTo(d1.getAggregateId())));
    }

    private DomainEvent createDomainEvent(Long version)
    {
        return createDomainEvent(new ObjectId().toHexString(), version);
    }

    private DomainEvent createDomainEvent(String id, Long version)
    {
        DomainEvent domainEvent = new DomainEvent();
        domainEvent.setAggregateId(id);
        domainEvent.setVersion(version);

        return domainEvent;
    }

    private Snapshot createSnapshot(String aggregateId, Long version)
    {
        Snapshot snapshot = new Snapshot();
        snapshot.setAggregateId(aggregateId);
        snapshot.setVersion(version);
        snapshot.setClassIdentifier("Snapshot");

        return snapshot;
    }
}