package org.c4.eventstore.domainevent;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bson.types.ObjectId;
import org.c4.eventstore.domainevent.persistence.SpringDataDomainEventRepository;
import org.c4.eventstore.snapshotting.Snapshot;
import org.c4.eventstore.snapshotting.persistence.SpringDataSnapshotRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DomainEventControllerTest
{
    private static final String GET_LIST_URL      = "/events/{aggregateId}/list";
    private static final String GET_STREAM_URL    = "/events/{aggregateId}/stream";
    private static final String GET_AGGREGATE_IDS = "/events/ids";

    @Autowired
    SpringDataDomainEventRepository springDataDomainEventRepository;
    @Autowired
    SpringDataSnapshotRepository    springDataSnapshotRepository;

    @Autowired
    private MockMvc      mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    private final String TEST_PROPERTY = "testProperty";

    @BeforeEach
    void setUp()
    {
        springDataDomainEventRepository.deleteAll();
        springDataSnapshotRepository.deleteAll();
    }

    /**
     * Backends können eine Liste von Events für ein Aggregat erhalten.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "backend")
    void getListOfEventsForAggregate_backendSuccess() throws Exception
    {
        DomainEvent d1 = createDomainEvent(1L);
        DomainEvent d2 = createDomainEvent(d1.getAggregateId(), 2L);
        DomainEvent d3 = createDomainEvent(0L);
        springDataDomainEventRepository.insert(d1);
        springDataDomainEventRepository.insert(d2);
        springDataDomainEventRepository.insert(d3);

        mockMvc.perform(get(GET_LIST_URL, d1.getAggregateId())).andExpect(status().isOk())
                .andExpect(jsonPath("$.content[*].version", containsInAnyOrder(1, 2)));
    }

    /**
     * Werden zu einem Aggregat keine Events gefunden, wird 404 zurückgegeben.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "backend")
    void getListOfEventsForAggregate_notFound() throws Exception
    {
        DomainEvent d1 = createDomainEvent(1L);
        springDataDomainEventRepository.insert(d1);

        mockMvc.perform(get(GET_LIST_URL, new ObjectId().toHexString())).andExpect(status().isNotFound());
    }

    /**
     * Eventstream wird korrekt zurückgegeben. Vorige Versionen werden ignoriert.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "backend")
    void getEventStreamForAggregate_withSnapshot() throws Exception
    {
        DomainEvent d0 = createDomainEvent(0L);
        d0.getPayload().put(TEST_PROPERTY, TEST_PROPERTY);
        DomainEvent d1 = createDomainEvent(d0.getAggregateId(), 1L);
        d1.getPayload().put(TEST_PROPERTY, TEST_PROPERTY);
        DomainEvent d2 = createDomainEvent(d0.getAggregateId(), 2L);
        Snapshot snapshot = createSnapshot(d0.getAggregateId(), 1L);
        springDataDomainEventRepository.insert(d0);
        springDataDomainEventRepository.insert(d1);
        springDataDomainEventRepository.insert(d2);
        springDataSnapshotRepository.insert(snapshot);

        mockMvc.perform(get(GET_STREAM_URL, d1.getAggregateId())).andExpect(status().isOk())
                .andExpect(jsonPath("$[*].version", contains(1, 2)))
                .andExpect(jsonPath("$[0].classIdentifier", is(equalTo("Snapshot"))))
                .andExpect(jsonPath("$[1].classIdentifier", is(equalTo("Event"))))
                .andExpect(jsonPath("$[*]." + TEST_PROPERTY).doesNotExist());
    }

    /**
     * Wenn kein Snapshot existiert, werden alle Events zurückgeliefert.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "backend")
    void getEventStreamForAggregate_withoutSnapshot() throws Exception
    {
        DomainEvent d0 = createDomainEvent(0L);
        d0.getPayload().put(TEST_PROPERTY, TEST_PROPERTY);
        DomainEvent d1 = createDomainEvent(d0.getAggregateId(), 1L);
        d1.getPayload().put(TEST_PROPERTY, TEST_PROPERTY);
        DomainEvent d2 = createDomainEvent(d0.getAggregateId(), 2L);
        springDataDomainEventRepository.insert(d0);
        springDataDomainEventRepository.insert(d1);
        springDataDomainEventRepository.insert(d2);

        mockMvc.perform(get(GET_STREAM_URL, d1.getAggregateId())).andExpect(status().isOk())
                .andExpect(jsonPath("$[*].version", contains(0, 1, 2)))
                .andExpect(jsonPath("$[*].classIdentifier", contains("Event", "Event", "Event")))
                .andExpect(jsonPath("$[*]." + TEST_PROPERTY, contains(TEST_PROPERTY, TEST_PROPERTY)));
    }

    /**
     * Wenn keine Events/Snapshot zu einem Aggregat existiert, wird 404 zurückgeliefert.
     */
    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "backend")
    void getEventstream_notFound() throws Exception
    {
        mockMvc.perform(get(GET_STREAM_URL, new ObjectId().toHexString())).andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "backend")
    void getAggregateIdsForEventTypes_notFound() throws Exception
    {
        mockMvc.perform(get(GET_AGGREGATE_IDS).param("eventTypes", "CREATED"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService", value = "backend")
    void getAggregateIdsForEventTypes_success() throws Exception
    {
        DomainEvent d1 = createDomainEvent(1L);
        d1.setClassIdentifier("CREATED");
        DomainEvent d2 = createDomainEvent(1L);
        d2.setClassIdentifier("CREATED");
        springDataDomainEventRepository.insert(Arrays.asList(d1, d2));

        mockMvc.perform(get(GET_AGGREGATE_IDS).param("eventTypes", "CREATED")).andExpect(status().isOk())
                .andExpect(jsonPath("$.stringList[*]", contains(d1.getAggregateId(), d2.getAggregateId())));
    }

    private DomainEvent createDomainEvent(Long version)
    {
        return createDomainEvent(new ObjectId().toHexString(), version);
    }

    private DomainEvent createDomainEvent(String id, Long version)
    {
        DomainEvent domainEvent = new DomainEvent();
        domainEvent.setAggregateId(id);
        domainEvent.setVersion(version);
        domainEvent.setClassIdentifier("Event");

        return domainEvent;
    }

    private Snapshot createSnapshot(String id, long version)
    {
        Snapshot snapshot = new Snapshot();
        snapshot.setAggregateId(id);
        snapshot.setClassIdentifier("Snapshot");
        snapshot.setVersion(version);

        return snapshot;
    }
}
