package org.c4.eventstore.domainevent;

import lombok.extern.log4j.Log4j2;
import org.c4.eventstore.aggregatelock.persistence.AggregateLockRepository;
import org.c4.eventstore.domainevent.persistence.DomainEventRepository;
import org.c4.eventstore.messaging.EventPublisher;
import org.c4.eventstore.snapshotting.SnapshottingService;
import org.springframework.kafka.annotation.KafkaListener;

/**
 * KafkaListener für alle {@link DomainEvent}s.
 * Events werden gespeichert, falls sie mit der Vorversion übereinstimmen.
 * Eingefügte Events werden über den Rückkanal an alle Services verteilt, die sich damit nach Belieben
 * aufdaten können.
 * <br/>
 * Copyright: Copyright (c) 17.12.2019 <br/>
 * Organisation: Exxeta GmbH
 *
 * @author Jan Buchholz <a href="mailto:jan.buchholz@exxeta.com">jan.buchholz@exxeta.com</a>
 */
@Log4j2
public class DomainEventSink
{
    private final SnapshottingService snapshottingService;

    private final DomainEventRepository   domainEventRepository;
    private final AggregateLockRepository aggregateLockRepository;

    private final EventPublisher successEventPublisher;
    private final EventPublisher versionMismatchEventPublisher;

    public DomainEventSink(SnapshottingService snapshottingService,
            DomainEventRepository domainEventRepository, AggregateLockRepository aggregateLockRepository,
            EventPublisher successEventPublisher, EventPublisher versionMismatchEventPublisher)
    {
        this.snapshottingService = snapshottingService;
        this.domainEventRepository = domainEventRepository;
        this.aggregateLockRepository = aggregateLockRepository;
        this.successEventPublisher = successEventPublisher;
        this.versionMismatchEventPublisher = versionMismatchEventPublisher;
    }

    @KafkaListener(topics = "domainEventToEventStoreTopic")
    public void consumeDomainEvent(DomainEvent domainEvent)
    {

        if (aggregateLockRepository.isNotLockedThenUpdate(domainEvent.getAggregateId()))
        {
            boolean eventHandled = handleDomainEvent(domainEvent);
            aggregateLockRepository.unlock(domainEvent.getAggregateId());
            if (eventHandled)
            {
                snapshottingService
                        .createSnapshotIfNecessary(domainEvent.getAggregateId(), domainEvent.getVersion());
            }
        }
        else
        {
            versionMismatchEventPublisher.send(domainEvent);

        }
    }

    private boolean handleDomainEvent(DomainEvent domainEvent)
    {
        try
        {
            Long domainEventVersion = domainEvent.getVersion();
            Long currentVersion = domainEventRepository
                    .getVersionForAggregateId(domainEvent.getAggregateId());

            if (currentVersion == null && domainEventVersion == null)
            {
                domainEvent.setVersion(0L);
                insertEvent(domainEvent);
            }
            else if (currentVersion != null && domainEventVersion != null
                    && currentVersion.longValue() == domainEventVersion.longValue())
            {
                domainEvent.setVersion(currentVersion + 1L);
                insertEvent(domainEvent);
            }
            else
            {
                if (log.isInfoEnabled())
                {
                    log.info(String.format("Version mismatch für '%s' auf Aggregat '%s'.",
                            domainEvent.getClassIdentifier(), domainEvent.getAggregateId()));
                }
                versionMismatchEventPublisher.send(domainEvent);
                return false;
            }
        }
        catch (Exception e)
        {
            if (log.isErrorEnabled())
            {
                log.error(String.format("Event '%s' des Aggregats '%s' konnte nicht verarbeitet werden.",
                        domainEvent.getClassIdentifier(), domainEvent.getAggregateId()));
                log.error(e);
            }
            return false;
            // Dieser Fall kann theoretisch nicht auftreten, wird aber vorsichtshalber durchgeführt,
            // um das Aufheben des Locks zu garantieren.
        }

        return true;
    }

    private void insertEvent(DomainEvent domainEvent)
    {
        DomainEvent insertedEvent = domainEventRepository.insert(domainEvent);
        successEventPublisher.send(insertedEvent);
    }
}
