package org.c4.eventstore.snapshotting.persistence;

import org.c4.eventstore.snapshotting.Snapshot;

import java.util.Optional;

/**
 * Repository für die Verwaltung von {@link Snapshot}s.
 * <br/>
 * Copyright: Copyright (c) 23.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface SnapshotRepository
{
    /**
     * Liefert den Snapshot für ein Aggregat zurück.
     */
    Optional<Snapshot> getSnapshot(String aggregateId);

    /**
     * Upserted einen Snapshot.
     */
    void save(Snapshot snapshot);
}
