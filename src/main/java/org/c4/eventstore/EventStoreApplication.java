package org.c4.eventstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;

@SpringBootApplication
@EnableWebSecurity
@EnableMongoAuditing
@EnableMongoRepositories
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class EventStoreApplication
{

    public static void main(String[] args)
    {
        SpringApplication.run(EventStoreApplication.class, args);
    }

}
