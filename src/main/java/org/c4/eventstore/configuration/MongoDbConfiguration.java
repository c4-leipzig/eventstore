package org.c4.eventstore.configuration;

import org.c4.eventstore.aggregatelock.persistence.AggregateLockRepository;
import org.c4.eventstore.aggregatelock.persistence.AggregateLockRepositoryImpl;
import org.c4.eventstore.aggregatelock.persistence.SpringDataAggregateLockRepository;
import org.c4.eventstore.domainevent.persistence.DomainEventRepository;
import org.c4.eventstore.domainevent.persistence.DomainEventRepositoryImpl;
import org.c4.eventstore.domainevent.persistence.SpringDataDomainEventRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.DefaultMongoTypeMapper;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

/**
 * Initialisierung der Mongo-Repositories.
 * <br/>
 * Copyright: Copyright (c) 19.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class MongoDbConfiguration
{
    /**
     * Überschreiben des Standardmappers, um unnötiges _class-Feld zu umgehen.
     */
    @Bean
    public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory)
    {
        DbRefResolver dbRefResolver = new DefaultDbRefResolver(mongoDbFactory);
        MappingMongoConverter mongoConverter = new MappingMongoConverter(dbRefResolver,
                new MongoMappingContext());
        mongoConverter.setTypeMapper(new DefaultMongoTypeMapper(null));

        return new MongoTemplate(mongoDbFactory, mongoConverter);
    }

    @Bean
    public DomainEventRepository domainEventRepository(
            SpringDataDomainEventRepository springDataDomainEventRepository)
    {
        return new DomainEventRepositoryImpl(springDataDomainEventRepository);
    }

    @Bean
    public AggregateLockRepository aggregateLockRepository(
            SpringDataAggregateLockRepository springDataAggregateLockRepository,
            MongoOperations mongoOperations)
    {
        return new AggregateLockRepositoryImpl(springDataAggregateLockRepository, mongoOperations);
    }
}
