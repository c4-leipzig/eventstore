package org.c4.eventstore.snapshotting.persistence;

import org.bson.types.ObjectId;
import org.c4.eventstore.snapshotting.Snapshot;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class SnapshotRepositoryImplTest
{
    @Autowired
    private SpringDataSnapshotRepository springDataSnapshotRepository;

    @Autowired
    SnapshotRepository snapshotRepository;

    @BeforeEach
    void setUp()
    {
        springDataSnapshotRepository.deleteAll();
    }

    /**
     * Ein Snapshot wird korrekt zurückgegeben.
     */
    @Test
    void getSnapshot()
    {
        Snapshot snapshot = createSnapshot(1L);
        springDataSnapshotRepository.save(snapshot);

        Optional<Snapshot> result = snapshotRepository.getSnapshot(snapshot.getAggregateId());

        assertTrue(result.isPresent());
        assertThat(result.get().getVersion(), is(equalTo(1L)));
    }

    /**
     * Speichern eines Snapshots.
     */
    @Test
    void save()
    {
        Snapshot snapshot = createSnapshot(1L);

        snapshotRepository.save(snapshot);

        Optional<Snapshot> result = springDataSnapshotRepository.findByAggregateId(snapshot.getAggregateId());

        assertTrue(result.isPresent());
    }

    /**
     * Bei einem erneuten Speichern wird der alte Datensatz überschrieben.
     */
    @Test
    void upsert()
    {
        Snapshot snapshot = createSnapshot(1L);
        snapshotRepository.save(snapshot);
        snapshot.setVersion(2L);
        snapshotRepository.save(snapshot);

        List<Snapshot> result = springDataSnapshotRepository.findAll();

        assertThat(result.size(), is(equalTo(1)));
        assertThat(result.get(0).getVersion(), is(equalTo(2L)));
    }

    private Snapshot createSnapshot(Long version)
    {
        Snapshot snapshot = new Snapshot();
        snapshot.setAggregateId(new ObjectId().toHexString());
        snapshot.setVersion(version);

        return snapshot;
    }
}