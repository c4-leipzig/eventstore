package org.c4.eventstore.domainevent;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;
import java.util.HashMap;
import java.util.Map;

/**
 * Repräsentation eines Events, das den Application State verändert.
 * <br/>
 * Copyright: Copyright (c) 19.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Document(collection = "domainEvents")
@Getter
@Setter
public class DomainEvent implements IDomainEvent
{
    @Id
    private String  id;
    private String  aggregateId;
    private String  createdBy;
    @CreatedDate
    private Instant createdAt;
    private Long    version;
    private String  classIdentifier;

    @Getter(value = AccessLevel.NONE)
    @Setter(value = AccessLevel.NONE)
    private Map<String, Object> payload = new HashMap<>();

    @JsonAnySetter
    public void setPayload(String name, Object value)
    {
        payload.put(name, value);
    }

    @JsonAnyGetter
    public Map<String, Object> getPayload()
    {
        return payload;
    }
}
