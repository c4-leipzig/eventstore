package org.c4.eventstore.configuration;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;
import java.util.Collections;

@Configuration
public class MockUserDetailsService
{
    @Bean
    @Primary
    public UserDetailsService userDetailsService()
    {
        User backendUser = new User("backend", "xxx",
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_backend")));

        User noRoleUser = new User("noroleuser", "xxx", Collections.emptyList());

        return new InMemoryUserDetailsManager(Arrays.asList(backendUser, noRoleUser));
    }
}
