package org.c4.eventstore.aggregatelock.persistence;

import org.c4.eventstore.aggregatelock.AggregateLockDto;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

/**
 * Implementation des {@link AggregateLockRepository}s.
 * <br/>
 * Copyright: Copyright (c) 18.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class AggregateLockRepositoryImpl implements AggregateLockRepository
{
    private final SpringDataAggregateLockRepository springDataAggregateLockRepository;
    private final MongoOperations                   mongoOperations;

    public AggregateLockRepositoryImpl(SpringDataAggregateLockRepository springDataAggregateLockRepository,
            MongoOperations mongoOperations)
    {
        this.springDataAggregateLockRepository = springDataAggregateLockRepository;
        this.mongoOperations = mongoOperations;
    }

    @Override
    public boolean isNotLockedThenUpdate(String aggregateId)
    {
        Query query = new Query(Criteria.where("aggregateId").is(aggregateId));

        AggregateLockDto oldLockDto = mongoOperations.findAndModify(query, Update.update("lockState", true),
                FindAndModifyOptions.options().upsert(true), AggregateLockDto.class);

        return oldLockDto == null || !oldLockDto.isLockState();
    }

    @Override
    public void unlock(String aggregateId)
    {
        AggregateLockDto lockDto = new AggregateLockDto();
        lockDto.setAggregateId(aggregateId);
        lockDto.setLockState(false);

        springDataAggregateLockRepository.save(lockDto);
    }
}
