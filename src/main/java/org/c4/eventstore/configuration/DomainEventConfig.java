package org.c4.eventstore.configuration;

import org.c4.eventstore.domainevent.DomainEventService;
import org.c4.eventstore.domainevent.persistence.DomainEventRepository;
import org.c4.eventstore.snapshotting.persistence.SnapshotRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Konfiguration für DomainEvents.
 * <br/>
 * Copyright: Copyright (c) 22.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class DomainEventConfig
{
    @Bean
    public DomainEventService domainEventService(DomainEventRepository domainEventRepository,
            SnapshotRepository snapshotRepository)
    {
        return new DomainEventService(domainEventRepository, snapshotRepository);
    }
}
