package org.c4.eventstore.domainevent;

import lombok.Getter;
import lombok.Setter;
import org.c4.eventstore.ErrorKey;

import javax.validation.constraints.NotEmpty;
import java.util.ArrayList;
import java.util.List;

/**
 * Objekt zur Repräsentation einer Abfrage von Aggregat-Ids.
 * <br/>
 * Copyright: Copyright (c) 30.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
public class DomainEventAggregateIdQuery
{
    @NotEmpty (message = ErrorKey.MISSING_EVENT_TYPES)
    private List<String> eventTypes = new ArrayList<>();
}
