package org.c4.eventstore.snapshotting.persistence;

import org.c4.eventstore.snapshotting.Snapshot;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * Data-Repository für die Verwaltung von {@link Snapshot}s.
 * <br/>
 * Copyright: Copyright (c) 23.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface SpringDataSnapshotRepository extends MongoRepository<Snapshot, String>
{
    /**
     * Liefert den Snapshot zu einem Aggregat
     */
    Optional<Snapshot> findByAggregateId(String aggregateId);
}
