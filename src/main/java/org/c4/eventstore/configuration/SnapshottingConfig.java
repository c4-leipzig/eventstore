package org.c4.eventstore.configuration;

import org.c4.eventstore.domainevent.DomainEventService;
import org.c4.eventstore.snapshotting.SnapshottingService;
import org.c4.eventstore.snapshotting.persistence.SnapshotRepository;
import org.c4.eventstore.snapshotting.persistence.SnapshotRepositoryImpl;
import org.c4.eventstore.snapshotting.persistence.SpringDataSnapshotRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Spring-Config für das Snapshotting von Aggregaten.
 * <br/>
 * Copyright: Copyright (c) 23.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Configuration
public class SnapshottingConfig
{
    @Value("${c4.eventstore.snapshot.amount:20}")
    private Long snapshotAmount;

    @Bean
    public SnapshotRepository snapshotRepository(SpringDataSnapshotRepository springDataSnapshotRepository)
    {
        return new SnapshotRepositoryImpl(springDataSnapshotRepository);
    }

    @Bean
    public SnapshottingService snapshottingService(SnapshotRepository snapshotRepository,
            DomainEventService domainEventService)
    {
        return new SnapshottingService(snapshotAmount, snapshotRepository, domainEventService);
    }
}
