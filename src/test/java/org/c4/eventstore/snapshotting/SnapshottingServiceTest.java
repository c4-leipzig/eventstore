package org.c4.eventstore.snapshotting;

import org.bson.types.ObjectId;
import org.c4.eventstore.domainevent.DomainEvent;
import org.c4.eventstore.domainevent.DomainEventService;
import org.c4.eventstore.domainevent.persistence.SpringDataDomainEventRepository;
import org.c4.eventstore.snapshotting.persistence.SnapshotRepository;
import org.c4.eventstore.snapshotting.persistence.SpringDataSnapshotRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Testet die Funktionalität des {@link SnapshottingService}s.
 * <br/>
 * Copyright: Copyright (c) 27.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@SpringBootTest
class SnapshottingServiceTest
{
    @Autowired
    private SpringDataSnapshotRepository    springDataSnapshotRepository;
    @Autowired
    private SnapshotRepository              snapshotRepository;
    @Autowired
    private SpringDataDomainEventRepository springDataDomainEventRepository;
    @Autowired
    private DomainEventService              domainEventService;

    private SnapshottingService snapshottingService;

    private final long   SNAPSHOT_AMOUNT      = 3;
    private final String TEST_KEY             = "testKey";
    private final String TEST_KEY_2           = "testKey2";
    private final String TEST_EVENT_QUALIFIER = "TestCreatedEvent";

    @BeforeEach
    void setUp()
    {
        springDataSnapshotRepository.deleteAll();
        springDataDomainEventRepository.deleteAll();
        snapshottingService = new SnapshottingService(SNAPSHOT_AMOUNT, snapshotRepository,
                domainEventService);
    }

    /**
     * Wenn die Version kein Vielfaches des Snapshot-Amounts ist, darf kein Snapshot erstellt werden.
     */
    @Test
    void createSnapshotIfNecessary_DoesNotCreateIfVersionDoesNotFit()
    {
        DomainEvent domainEvent = createDomainEvent(1).get(0);
        springDataDomainEventRepository.insert(domainEvent);

        snapshottingService.createSnapshotIfNecessary(domainEvent.getAggregateId(), 0L);

        Optional<Snapshot> result = snapshotRepository.getSnapshot(domainEvent.getAggregateId());

        assertFalse(result.isPresent());
    }

    /**
     * Wenn die Version passt, wird ein Snapshot erstellt.
     * Die aktuelle Test-Property wird korrekt persistiert.
     * Der ClassIdentifier wird entsprechend gesetzt.
     */
    @Test
    void createSnapshotIfNecessary_CreateIfVersionFits()
    {
        List<DomainEvent> domainEvents = createDomainEvent(4);
        springDataDomainEventRepository.insert(domainEvents);

        snapshottingService.createSnapshotIfNecessary(domainEvents.get(0).getAggregateId(), 3L);

        Optional<Snapshot> result = snapshotRepository.getSnapshot(domainEvents.get(0).getAggregateId());

        assertTrue(result.isPresent());
        assertThat(result.get().getPayload().get(TEST_KEY), is(equalTo(3)));
        assertThat(result.get().getClassIdentifier(), is(equalTo("Snapshot" + TEST_EVENT_QUALIFIER)));
    }

    /**
     * Existiert bereits ein Snapshot, muss dieser überschrieben werden.
     * Außerdem muss zur Erstellung des neuen Snapshots der alte Snapshot verwendet werden.
     */
    @Test
    void createFollowingSnapshot_usePrecedingSnapshotForAggregate()
    {
        List<DomainEvent> domainEvents = createDomainEvent(7);
        springDataDomainEventRepository.insert(domainEvents);

        Snapshot snapshot = createSnapshot(domainEvents.get(0).getAggregateId(), 3L);
        springDataSnapshotRepository.insert(snapshot);

        snapshottingService.createSnapshotIfNecessary(domainEvents.get(0).getAggregateId(), 6L);

        List<Snapshot> result = springDataSnapshotRepository.findAll();

        assertThat(result.size(), is(equalTo(1)));
        assertThat(result.get(0).getAggregateId(), is(equalTo(domainEvents.get(0).getAggregateId())));
        assertThat(result.get(0).getPayload().get(TEST_KEY), is(equalTo(6)));
        assertThat(result.get(0).getPayload().get(TEST_KEY_2), is(equalTo(TEST_KEY_2)));
    }

    /**
     * Sollte ein Snapshot nicht erstellt worden sein, muss im nächsten Zyklus wie gewohnt fortgefahren werden.
     */
    @Test
    void createFollowingSnapshot_missingSnapshotIsIgnored()
    {
        List<DomainEvent> domainEvents = createDomainEvent(10);
        springDataDomainEventRepository.insert(domainEvents);

        Snapshot snapshot = createSnapshot(domainEvents.get(0).getAggregateId(), 3L);
        springDataSnapshotRepository.insert(snapshot);

        snapshottingService.createSnapshotIfNecessary(domainEvents.get(0).getAggregateId(), 9L);

        Optional<Snapshot> result = springDataSnapshotRepository
                .findByAggregateId(domainEvents.get(0).getAggregateId());

        assertTrue(result.isPresent());
        assertThat(result.get().getVersion(), is(equalTo(9L)));
        assertThat(result.get().getPayload().get(TEST_KEY_2), is(equalTo(TEST_KEY_2)));
    }

    /**
     * Sollte ein Snapshot (für V6) nicht erstellt worden sein,
     * wird im nächsten Schritt (V7) trotzdem kein Snapshot angelegt.
     */
    @Test
    void doNotCreateFollowingSnapshotIfSnapshotIsMissing()
    {
        List<DomainEvent> domainEvents = createDomainEvent(8);
        springDataDomainEventRepository.insert(domainEvents);

        Snapshot snapshot = createSnapshot(domainEvents.get(0).getAggregateId(), 3L);
        springDataSnapshotRepository.insert(snapshot);

        snapshottingService.createSnapshotIfNecessary(domainEvents.get(0).getAggregateId(), 7L);

        Optional<Snapshot> result = springDataSnapshotRepository
                .findByAggregateId(domainEvents.get(0).getAggregateId());

        assertTrue(result.isPresent());
        assertThat(result.get().getVersion(), is(equalTo(3L)));
        assertThat(result.get().getPayload().get(TEST_KEY_2), is(equalTo(TEST_KEY_2)));
    }

    /**
     * Wenn zu dem Aggregat keine Events existieren, wird abgebrochen.
     */
    @Test
    void doNotCreateSnapshotIfAggregateNotPresent()
    {
        snapshottingService.createSnapshotIfNecessary("nonExistent", 3L);

        Optional<Snapshot> result = springDataSnapshotRepository.findByAggregateId("nonExistent");

        assertFalse(result.isPresent());
    }

    private List<DomainEvent> createDomainEvent(int amount)
    {
        List<DomainEvent> result = new ArrayList<>();

        String id = new ObjectId().toHexString();
        for (int i = 0; i < amount; i++)
        {
            DomainEvent domainEvent = new DomainEvent();
            domainEvent.setAggregateId(id);
            domainEvent.setClassIdentifier(TEST_EVENT_QUALIFIER);
            domainEvent.setVersion((long) i);
            domainEvent.getPayload().put(TEST_KEY, i);
            result.add(domainEvent);
        }

        return result;
    }

    private Snapshot createSnapshot(String id, long version)
    {
        Snapshot snapshot = new Snapshot();
        snapshot.setAggregateId(id);
        snapshot.setClassIdentifier("Snapshot" + TEST_EVENT_QUALIFIER);
        snapshot.setVersion(version);
        snapshot.getPayload().put(TEST_KEY_2, TEST_KEY_2);

        return snapshot;
    }
}