package org.c4.eventstore.aggregatelock.persistence;

import org.bson.types.ObjectId;
import org.c4.eventstore.aggregatelock.AggregateLockDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class AggregateLockRepositoryImplTest
{

    @Autowired
    AggregateLockRepository           aggregateLockRepository;
    @Autowired
    SpringDataAggregateLockRepository springDataAggregateLockRepository;

    @BeforeEach
    void setUp()
    {
        springDataAggregateLockRepository.deleteAll();
    }

    /**
     * Für ein gelocktes Aggregat muss "false" zurückgegeben werden und der Status erhalten bleiben.
     */
    @Test
    void lockedAggregateReturnsFalse()
    {
        AggregateLockDto lockDto = createLock(true);
        springDataAggregateLockRepository.insert(lockDto);

        boolean result = aggregateLockRepository.isNotLockedThenUpdate(lockDto.getAggregateId());
        Optional<AggregateLockDto> currentLock = springDataAggregateLockRepository
                .findById(lockDto.getAggregateId());

        assertFalse(result);
        assertTrue(currentLock.isPresent());
        assertTrue(currentLock.get().isLockState());
    }

    /**
     * Für ein nicht gelocktes Aggregat muss "true" zurückgegeben und der Status gesetzt werden.
     */
    @Test
    void unlockedAggregateReturnsTrueAndGetsLocked()
    {
        AggregateLockDto lockDto = createLock(false);
        springDataAggregateLockRepository.insert(lockDto);

        boolean result = aggregateLockRepository.isNotLockedThenUpdate(lockDto.getAggregateId());
        Optional<AggregateLockDto> currentLock = springDataAggregateLockRepository
                .findById(lockDto.getAggregateId());

        assertTrue(result);
        assertTrue(currentLock.isPresent());
        assertTrue(currentLock.get().isLockState());
    }

    /**
     * Existiert noch kein Lock, muss "true" zurückgegeben und das Lock erstellt werden.
     */
    @Test
    void aggregateNotYetPresentGetsLocked()
    {
        String newAggregateId = new ObjectId().toHexString();
        boolean result = aggregateLockRepository.isNotLockedThenUpdate(newAggregateId);
        Optional<AggregateLockDto> currentLock = springDataAggregateLockRepository
                .findById(newAggregateId);

        assertTrue(result);
        assertTrue(currentLock.isPresent());
        assertTrue(currentLock.get().isLockState());
    }

    /**
     * Unlock eines aktiven Locks.
     */
    @Test
    void unlock()
    {
        AggregateLockDto lockDto = createLock(true);
        springDataAggregateLockRepository.insert(lockDto);
        aggregateLockRepository.unlock(lockDto.getAggregateId());

        Optional<AggregateLockDto> currentLock = springDataAggregateLockRepository
                .findById(lockDto.getAggregateId());

        assertTrue(currentLock.isPresent());
        assertFalse(currentLock.get().isLockState());
    }

    private AggregateLockDto createLock(boolean lock)
    {
        AggregateLockDto lockDto = new AggregateLockDto();
        lockDto.setAggregateId(new ObjectId().toHexString());
        lockDto.setLockState(lock);

        return lockDto;
    }
}