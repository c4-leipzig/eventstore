package org.c4.eventstore.domainevent;

import org.c4.eventstore.StringListDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * Controller zur Abfrage von Eventstreams.
 * <br/>
 * Copyright: Copyright (c) 22.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@RestController
public class DomainEventController
{
    private final DomainEventService domainEventService;

    public DomainEventController(DomainEventService domainEventService)
    {
        this.domainEventService = domainEventService;
    }

    /**
     * Liefert für ein Aggregat eine Auswahl von Events zurück.
     */
    @GetMapping("/events/{aggregateId:[0-9a-f]{24}}/list")
    @PreAuthorize("hasRole('backend')")
    public ResponseEntity<Page<DomainEvent>> getEventsForAggreagetId(
            @PageableDefault(value = Integer.MAX_VALUE, sort = { "aggregateId" }) Pageable pageable,
            @PathVariable(name = "aggregateId") String aggregatId)
    {
        Page<DomainEvent> domainEvents = domainEventService
                .getDomainEventsForAggregateId(aggregatId, pageable);

        if (domainEvents.getNumberOfElements() == 0)
        {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(domainEvents);
    }

    /**
     * Liefert für ein Aggregat den gesamten EventStream (inkl. Snapshot) zurück.
     */
    @GetMapping("/events/{aggregateId:[0-9a-f]{24}}/stream")
    @PreAuthorize("hasRole('backend')")
    public ResponseEntity<List<IDomainEvent>> getEventStreamForAggregateId(
            @PathVariable(name = "aggregateId") String aggregateId)
    {
        List<IDomainEvent> eventStream = domainEventService.getEventStreamForAggregateId(aggregateId);

        if (eventStream.isEmpty())
        {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(eventStream);
    }

    /**
     * Liefert für eine Liste von Events alle zugehörigen Aggregate zurück.
     * Da die Methode nur intern verwendet wird, wird keine Duplikatprüfung durchgeführt.
     */
    @GetMapping("/events/ids")
    @PreAuthorize("hasRole('backend')")
    public ResponseEntity<StringListDto> getAggregateIdsForEventTypes(
            @Valid DomainEventAggregateIdQuery query)
    {
        List<String> result = domainEventService.getAggregateIdsForEventTypes(query);

        if (result.isEmpty())
        {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(new StringListDto(result));
    }
}
