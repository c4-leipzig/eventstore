package org.c4.eventstore.domainevent;

import org.c4.eventstore.snapshotting.Snapshot;

import java.util.Map;

/**
 * Interface für die Zusammenfassung von {@link DomainEvent}s und {@link Snapshot}s in eine Gruppe.
 * <br/>
 * Copyright: Copyright (c) 23.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface IDomainEvent
{
    Long getVersion();

    Map<String, Object> getPayload();

    String getAggregateId();

    String getClassIdentifier();
}
