package org.c4.eventstore.snapshotting;

import org.c4.eventstore.domainevent.DomainEvent;
import org.c4.eventstore.domainevent.DomainEventService;
import org.c4.eventstore.domainevent.IDomainEvent;
import org.c4.eventstore.snapshotting.persistence.SnapshotRepository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Service zur Verwaltung von {@link Snapshot}s.
 * <br/>
 * Copyright: Copyright (c) 23.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class SnapshottingService
{
    private final Long snapshotAmount;

    private final SnapshotRepository snapshotRepository;
    private final DomainEventService domainEventService;

    public SnapshottingService(Long snapshotAmount, SnapshotRepository snapshotRepository,
            DomainEventService domainEventService)
    {
        this.snapshotAmount = snapshotAmount;
        this.snapshotRepository = snapshotRepository;
        this.domainEventService = domainEventService;
    }

    public void createSnapshotIfNecessary(String aggregateId, Long version)
    {
        if (!snapshotNeeded(version))
        {
            return;
        }

        List<IDomainEvent> eventStream = domainEventService.getEventStreamForAggregateId(aggregateId);
        if (eventStream.isEmpty())
        {
            return;
        }

        Snapshot snapshot = createSnapshot(eventStream, version);
        snapshotRepository.save(snapshot);
    }

    private boolean snapshotNeeded(Long version)
    {
        return version > 0 && version % snapshotAmount == 0;
    }

    private Snapshot createSnapshot(List<IDomainEvent> eventStream, Long version)
    {
        Map<String, Object> mergedPayload = new HashMap<>();
        for (IDomainEvent event : eventStream)
        {
            mergedPayload.putAll(event.getPayload());
        }

        IDomainEvent initialEvent = eventStream.get(0);
        String classIdentifier;
        if (initialEvent instanceof DomainEvent)
        {
            classIdentifier = "Snapshot" + initialEvent.getClassIdentifier();
        }
        else {
            classIdentifier = initialEvent.getClassIdentifier();
        }

        Snapshot snapshot = new Snapshot();
        snapshot.setAggregateId(initialEvent.getAggregateId());
        snapshot.setVersion(version);
        snapshot.setPayload(mergedPayload);
        snapshot.setClassIdentifier(classIdentifier);

        return snapshot;
    }
}
