package org.c4.eventstore.aggregatelock;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

/**
 * Repräsentation eines AggregatLocks.
 * Vor der Veränderung eines Aggregats wird es gelockt, um eine simultane Verarbeitung mehrerer Events auf einem
 * Aggregat zu verhindern (pessimistic locking).
 * <br/>
 * Copyright: Copyright (c) 19.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
@Getter
@Setter
public class AggregateLockDto
{
    @Id
    private String aggregateId;

    private boolean lockState;
}
