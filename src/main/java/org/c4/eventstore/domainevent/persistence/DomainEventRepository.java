package org.c4.eventstore.domainevent.persistence;

import org.c4.eventstore.domainevent.DomainEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Repository zur Verwaltung von {@link DomainEvent}s.
 * <br/>
 * Copyright: Copyright (c) 19.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface DomainEventRepository
{
    /**
     * Fügt ein neues DomainEvent hinzu.
     */
    DomainEvent insert(DomainEvent domainEvent);

    /**
     * Liest für ein Aggregat die aktuelle Version aus.
     */
    Long getVersionForAggregateId(String aggregateId);

    /**
     * Gibt eine Liste von Events für ein Aggregate zurück.
     */
    Page<DomainEvent> getDomainEventsForAggregateId(String aggregateId, Pageable pageable);

    /**
     * Gibt alle Events für ein Aggregat ab einer initialen Version aus.
     */
    List<DomainEvent> getDomainEventsForAggregateId(String aggregateId, Long initialVersion);

    List<String> getAggregateIdsForEventTypes(List<String> eventTypes);
}
