package org.c4.eventstore.domainevent;

import org.c4.eventstore.domainevent.persistence.DomainEventRepository;
import org.c4.eventstore.snapshotting.Snapshot;
import org.c4.eventstore.snapshotting.persistence.SnapshotRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service für die Verwaltung von {@link DomainEvent}s.
 * <br/>
 * Copyright: Copyright (c) 22.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class DomainEventService
{
    private final DomainEventRepository domainEventRepository;
    private final SnapshotRepository snapshotRepository;

    public DomainEventService(DomainEventRepository domainEventRepository,
            SnapshotRepository snapshotRepository)
    {
        this.domainEventRepository = domainEventRepository;
        this.snapshotRepository = snapshotRepository;
    }

    public Page<DomainEvent> getDomainEventsForAggregateId(String aggregateId, Pageable pageable)
    {
        return domainEventRepository.getDomainEventsForAggregateId(aggregateId, pageable);
    }

    public List<IDomainEvent> getEventStreamForAggregateId(String aggregateId)
    {
        Optional<Snapshot> maybeSnapshot = snapshotRepository.getSnapshot(aggregateId);

        List<IDomainEvent> result = new ArrayList<>();
        Long minimumVersion = 0L;

        if (maybeSnapshot.isPresent())
        {
            result.add(maybeSnapshot.get());
            minimumVersion = maybeSnapshot.get().getVersion() + 1L;
        }

        result.addAll(domainEventRepository.getDomainEventsForAggregateId(aggregateId, minimumVersion));

        return result;
    }

    public List<String> getAggregateIdsForEventTypes(DomainEventAggregateIdQuery query)
    {
        return domainEventRepository.getAggregateIdsForEventTypes(query.getEventTypes());
    }
}
