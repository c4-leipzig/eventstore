package org.c4.eventstore.snapshotting.persistence;

import org.c4.eventstore.snapshotting.Snapshot;

import java.util.Optional;

/**
 * Mongo-Implementierung des {@link SnapshotRepository}s.
 * <br/>
 * Copyright: Copyright (c) 23.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class SnapshotRepositoryImpl implements SnapshotRepository
{
    private final SpringDataSnapshotRepository springDataSnapshotRepository;

    public SnapshotRepositoryImpl(SpringDataSnapshotRepository springDataSnapshotRepository)
    {
        this.springDataSnapshotRepository = springDataSnapshotRepository;
    }

    @Override
    public Optional<Snapshot> getSnapshot(String aggregateId)
    {
        return springDataSnapshotRepository.findByAggregateId(aggregateId);
    }

    @Override
    public void save(Snapshot snapshot)
    {
        springDataSnapshotRepository.save(snapshot);
    }
}
