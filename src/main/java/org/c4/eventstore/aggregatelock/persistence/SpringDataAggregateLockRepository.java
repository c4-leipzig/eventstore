package org.c4.eventstore.aggregatelock.persistence;

import org.c4.eventstore.aggregatelock.AggregateLockDto;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * SpringData-Repository für die Verwaltung des Lockstatus von Aggregaten.
 * <br/>
 * Copyright: Copyright (c) 19.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface SpringDataAggregateLockRepository extends MongoRepository<AggregateLockDto, String>
{
}
