package org.c4.eventstore.domainevent.persistence;

import org.bson.types.ObjectId;
import org.c4.eventstore.domainevent.DomainEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class DomainEventRepositoryImplTest
{
    @Autowired
    SpringDataDomainEventRepository springDataDomainEventRepository;
    @Autowired
    DomainEventRepository           domainEventRepository;

    @BeforeEach
    void setUp()
    {
        springDataDomainEventRepository.deleteAll();
    }

    /**
     * Einfügen eines Events.
     */
    @Test
    void insert()
    {
        DomainEvent domainEvent = createDomainEvent(null);
        domainEventRepository.insert(domainEvent);

        Optional<DomainEvent> result = springDataDomainEventRepository
                .findFirstByAggregateIdOrderByVersionDesc(domainEvent.getAggregateId());

        assertTrue(result.isPresent());
        assertThat(result.get().getVersion(), equalTo(null));
    }

    /**
     * Version korrekt auslesen.
     */
    @Test
    void getVersion()
    {
        DomainEvent domainEvent = createDomainEvent(1L);
        springDataDomainEventRepository.insert(domainEvent);

        Long result = domainEventRepository.getVersionForAggregateId(domainEvent.getAggregateId());

        assertNotNull(result);
        assertThat(result, equalTo(1L));
    }

    /**
     * Existiert das Aggregat noch nicht, muss die Version als null angegeben werden.
     */
    @Test
    void getVersionNotPresent()
    {
        Long result = domainEventRepository.getVersionForAggregateId(new ObjectId().toHexString());

        assertNull(result);
    }

    /**
     * Es müssen alle Events zurückgegeben werden.
     */
    @Test
    void getEventsForAggregateId()
    {
        DomainEvent domainEvent = createDomainEvent(1L);
        DomainEvent falseDomainEvent = createDomainEvent(0L);
        springDataDomainEventRepository.insert(domainEvent);
        springDataDomainEventRepository.insert(falseDomainEvent);
        domainEvent.setId(null);
        domainEvent.setVersion(2L);
        springDataDomainEventRepository.insert(domainEvent);

        Page<DomainEvent> result = domainEventRepository
                .getDomainEventsForAggregateId(domainEvent.getAggregateId(),
                        PageRequest.of(0, 20, Sort.by("aggregateId")));
        List<Long> resultVersionList = result.get().map(DomainEvent::getVersion).collect(Collectors.toList());

        assertThat(result.getTotalElements(), equalTo(2L));
        assertThat(resultVersionList, containsInAnyOrder(1L, 2L));
    }

    /**
     * Es dürfen nur Events zurückgegeben werden, deren Version groß genug ist.
     */
    @Test
    void getEventsForAggregateIdVersionGreaterEqual()
    {
        DomainEvent domainEvent = createDomainEvent(0L);
        DomainEvent domainEvent1 = createDomainEvent(domainEvent.getAggregateId(), 1L);
        DomainEvent domainEvent2 = createDomainEvent(domainEvent.getAggregateId(), 2L);
        springDataDomainEventRepository.insert(domainEvent);
        springDataDomainEventRepository.insert(domainEvent1);
        springDataDomainEventRepository.insert(domainEvent2);

        List<Long> result = domainEventRepository
                .getDomainEventsForAggregateId(domainEvent.getAggregateId(), 1L).stream()
                .map(DomainEvent::getVersion).collect(Collectors.toList());

        assertThat(result.size(), is(equalTo(2)));
        assertThat(result, contains(1L, 2L));
    }

    /**
     * Zu den jeweiligen ClassIdentifieren werden die Ids zurückgegeben.
     * Andere Ids werden nicht zurückgegeben.
     */
    @Test
    void getAggregateIdsForEventTypes_success()
    {
        DomainEvent created1 = createDomainEvent(1L);
        created1.setClassIdentifier("CREATED");
        DomainEvent altered1 = createDomainEvent(created1.getAggregateId(), 2L);
        altered1.setClassIdentifier("ALTERED");
        DomainEvent created2 = createDomainEvent(1L);
        created2.setClassIdentifier("CREATED2");
        DomainEvent created3 = createDomainEvent(1L);
        created3.setClassIdentifier("CREATED3");
        springDataDomainEventRepository.insert(created1);
        springDataDomainEventRepository.insert(altered1);
        springDataDomainEventRepository.insert(created2);
        springDataDomainEventRepository.insert(created3);

        List<String> result = domainEventRepository
                .getAggregateIdsForEventTypes(Arrays.asList("CREATED", "CREATED2"));

        assertThat(result.size(), is(equalTo(2)));
        assertThat(result, contains(created1.getAggregateId(), created2.getAggregateId()));
    }

    /**
     * Werden keine Ergebnisse gefunden, wird eine leere Liste zurückgegeben.
     */
    @Test
    void getAggregateIdsForEventTypes_notFound()
    {
        DomainEvent altered1 = createDomainEvent(1L);
        altered1.setClassIdentifier("ALTERED");

        List<String> result = domainEventRepository
                .getAggregateIdsForEventTypes(Arrays.asList("CREATED"));

        assertTrue(result.isEmpty());
    }

    private DomainEvent createDomainEvent(Long version)
    {
        return createDomainEvent(new ObjectId().toHexString(), version);
    }

    private DomainEvent createDomainEvent(String id, Long version)
    {
        DomainEvent domainEvent = new DomainEvent();
        domainEvent.setAggregateId(id);
        domainEvent.setVersion(version);

        return domainEvent;
    }
}