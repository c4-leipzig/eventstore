package org.c4.eventstore.domainevent.persistence;

import org.c4.eventstore.domainevent.DomainEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Implementierung des {@link DomainEventRepository}s.
 * <br/>
 * Copyright: Copyright (c) 19.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public class DomainEventRepositoryImpl implements DomainEventRepository
{
    private final SpringDataDomainEventRepository springDataDomainEventRepository;

    public DomainEventRepositoryImpl(SpringDataDomainEventRepository springDataDomainEventRepository)
    {
        this.springDataDomainEventRepository = springDataDomainEventRepository;
    }

    @Override
    public DomainEvent insert(DomainEvent domainEvent)
    {
        return springDataDomainEventRepository.save(domainEvent);
    }

    @Override
    public Long getVersionForAggregateId(String aggregateId)
    {
        return springDataDomainEventRepository.findFirstByAggregateIdOrderByVersionDesc(aggregateId)
                .map(DomainEvent::getVersion).orElse(null);
    }

    @Override
    public Page<DomainEvent> getDomainEventsForAggregateId(String aggregateId, Pageable pageable)
    {
        return springDataDomainEventRepository.findAllByAggregateId(aggregateId, pageable);
    }

    @Override
    public List<DomainEvent> getDomainEventsForAggregateId(String aggregateId, Long initialVersion)
    {
        return springDataDomainEventRepository
                .findAllByAggregateIdAndVersionIsGreaterThanEqual(aggregateId, initialVersion);
    }

    @Override
    public List<String> getAggregateIdsForEventTypes(List<String> eventTypes)
    {
        List<DomainEvent> domainEventsOfTypes = springDataDomainEventRepository
                .findAllByClassIdentifierIn(eventTypes);

        return domainEventsOfTypes.stream().map(DomainEvent::getAggregateId)
                .collect(Collectors.toList());
    }
}
