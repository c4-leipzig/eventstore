package org.c4.eventstore.domainevent.persistence;

import org.c4.eventstore.domainevent.DomainEvent;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

/**
 * SpringData-Repository für die Verwaltung von {@link DomainEvent}s.
 * <br/>
 * Copyright: Copyright (c) 19.12.2019 <br/>
 * Organisation: Verein „C4“ e.V.
 *
 * @author Jan „Leonard“ Buchholz <a href="mailto:leonard@c4-leipzig.de">leonard@c4-leipzig.de</a>
 */
public interface SpringDataDomainEventRepository extends MongoRepository<DomainEvent, String>
{
    /**
     * Liefert das zuletzt hinzugefügte Event zu einem Aggregat
     */
    Optional<DomainEvent> findFirstByAggregateIdOrderByVersionDesc(String aggregateId);

    /**
     * Liefert eine Liste von Events zu einem Aggregat
     */
    Page<DomainEvent> findAllByAggregateId(String aggregateId, Pageable pageable);

    /**
     * Liefert alle Events zu einem Aggregate ab einer bestimmten Version zurück
     */
    List<DomainEvent> findAllByAggregateIdAndVersionIsGreaterThanEqual(String aggregateId, Long version);

    /**
     * Liefert alle Events zurück, die einen entsprechenden ClassIdentifier aufweisen (d.h. bestimmte Events).
     */
    List<DomainEvent> findAllByClassIdentifierIn(List<String> classIdentifier);
}
