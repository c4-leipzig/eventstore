package org.c4.eventstore.messaging;

import org.bson.types.ObjectId;
import org.c4.eventstore.aggregatelock.AggregateLockDto;
import org.c4.eventstore.aggregatelock.persistence.SpringDataAggregateLockRepository;
import org.c4.eventstore.configuration.TestMessagingConfig;
import org.c4.eventstore.domainevent.DomainEvent;
import org.c4.eventstore.domainevent.DomainEventSink;
import org.c4.eventstore.domainevent.persistence.SpringDataDomainEventRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@SpringBootTest
@EnableAutoConfiguration(exclude = KafkaAutoConfiguration.class)
@Import(TestMessagingConfig.class)
class DomainEventSinkTest
{
    @MockBean
    @Qualifier("successEventPublisher")
    private EventPublisher successEventPublisher;
    @MockBean
    @Qualifier("versionMismatchEventPublisher")
    private EventPublisher versionMismatchEventPublisher;

    @Autowired
    private SpringDataDomainEventRepository   springDataDomainEventRepository;
    @Autowired
    private SpringDataAggregateLockRepository springDataAggregateLockRepository;

    @Autowired
    private DomainEventSink domainEventSink;

    @BeforeEach
    void setUp()
    {
        springDataAggregateLockRepository.deleteAll();
        springDataDomainEventRepository.deleteAll();
    }

    /**
     * Wenn kein Lock existiert, wird das Event eingefügt und weitergeschickt.
     * Die Version ist initial null und wird auf 0 gesetzt.
     */
    @Test
    void noLockSaveAndResendVersionNull()
    {
        DomainEvent domainEvent = createDomainEvent();

        domainEventSink.consumeDomainEvent(domainEvent);

        Optional<AggregateLockDto> lock = springDataAggregateLockRepository
                .findById(domainEvent.getAggregateId());
        Optional<DomainEvent> insertedEvent = springDataDomainEventRepository
                .findFirstByAggregateIdOrderByVersionDesc(domainEvent.getAggregateId());

        assertTrue(lock.isPresent());
        assertFalse(lock.get().isLockState());
        assertTrue(insertedEvent.isPresent());
        assertThat(insertedEvent.get().getAggregateId(), is(equalTo(domainEvent.getAggregateId())));
        assertThat(insertedEvent.get().getVersion(), is(equalTo(0L)));
        verify(successEventPublisher, times(1)).send(any());
        verify(versionMismatchEventPublisher, never()).send(any());
    }

    /**
     * Wenn kein Lock existiert, wird das Event eingefügt und weitergeschickt.
     * Die Version ist eine Zahl und wird um eins inkrementiert.
     */
    @Test
    void noLockSaveAndResendVersionIsNumber()
    {
        DomainEvent domainEvent = createDomainEvent();
        long version = Math.round(Math.random() * 1000000);
        domainEvent.setVersion(version);
        springDataDomainEventRepository.insert(domainEvent);
        domainEvent.setCreatedBy("Peter Müller");

        domainEventSink.consumeDomainEvent(domainEvent);

        Optional<AggregateLockDto> lock = springDataAggregateLockRepository
                .findById(domainEvent.getAggregateId());
        Optional<DomainEvent> insertedEvent = springDataDomainEventRepository
                .findFirstByAggregateIdOrderByVersionDesc(domainEvent.getAggregateId());

        assertTrue(lock.isPresent());
        assertFalse(lock.get().isLockState());
        assertTrue(insertedEvent.isPresent());
        assertThat(insertedEvent.get().getAggregateId(), is(equalTo(domainEvent.getAggregateId())));
        assertThat(insertedEvent.get().getVersion(), is(equalTo(version + 1)));
        assertThat(insertedEvent.get().getCreatedBy(), is(equalTo("Peter Müller")));
        verify(successEventPublisher, times(1)).send(any());
        verify(versionMismatchEventPublisher, never()).send(any());
    }

    /**
     * Existiert ein Lock, wird keine Änderung vorgenommen und das Event an den Service zurückgegeben.
     */
    @Test
    void lockPresentDoNotSaveAndResubmit()
    {
        DomainEvent domainEvent = createDomainEvent();
        AggregateLockDto lockDto = new AggregateLockDto();
        lockDto.setLockState(true);
        lockDto.setAggregateId(domainEvent.getAggregateId());

        springDataAggregateLockRepository.insert(lockDto);

        domainEventSink.consumeDomainEvent(domainEvent);

        Optional<AggregateLockDto> insertedLock = springDataAggregateLockRepository
                .findById(domainEvent.getAggregateId());
        Optional<DomainEvent> insertedEvent = springDataDomainEventRepository
                .findFirstByAggregateIdOrderByVersionDesc(domainEvent.getAggregateId());

        assertTrue(insertedLock.isPresent());
        assertTrue(insertedLock.get().isLockState());
        assertFalse(insertedEvent.isPresent());
        verify(successEventPublisher, never()).send(any());
        verify(versionMismatchEventPublisher, times(1)).send(any());
    }

    /**
     * Bei einem Version Mismatch wird das Event nicht persistiert und zurückgegeben.
     * Der Lockstatus wird aufgehoben.
     */
    @Test
    void versionMismatchDoNotInsertAndResubmit()
    {
        DomainEvent domainEvent = createDomainEvent();
        domainEvent.setVersion(0L);
        springDataDomainEventRepository.insert(domainEvent);
        domainEvent.setVersion(5L);
        domainEvent.setCreatedBy("Peter Müller");

        domainEventSink.consumeDomainEvent(domainEvent);

        Optional<AggregateLockDto> insertedLock = springDataAggregateLockRepository
                .findById(domainEvent.getAggregateId());
        Optional<DomainEvent> insertedEvent = springDataDomainEventRepository
                .findFirstByAggregateIdOrderByVersionDesc(domainEvent.getAggregateId());

        assertTrue(insertedLock.isPresent());
        assertFalse(insertedLock.get().isLockState());
        assertTrue(insertedEvent.isPresent());
        assertThat(insertedEvent.get().getVersion(), is(equalTo(0L)));
        assertNull(insertedEvent.get().getCreatedBy());
        verify(successEventPublisher, never()).send(any());
        verify(versionMismatchEventPublisher, times(1)).send(any());
    }

    @Test
    void exceptionThrownLockStatusGetsReset()
    {
        DomainEvent domainEvent = createDomainEvent();

        doThrow(new IllegalArgumentException()).when(successEventPublisher).send(any());

        domainEventSink.consumeDomainEvent(domainEvent);

        Optional<AggregateLockDto> insertedLock = springDataAggregateLockRepository
                .findById(domainEvent.getAggregateId());

        assertTrue(insertedLock.isPresent());
        assertFalse(insertedLock.get().isLockState());
    }

    private DomainEvent createDomainEvent()
    {
        DomainEvent domainEvent = new DomainEvent();
        domainEvent.setAggregateId(new ObjectId().toHexString());

        return domainEvent;
    }
}